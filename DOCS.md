# dna v0.0.0



- [Mutation](#mutation)
	- [detect mutation](#detect-mutation)
	
- [Stat](#stat)
	- [Create stat](#create-stat)
	- [Delete stat](#delete-stat)
	- [Retrieve stat](#retrieve-stat)
	- [Retrieve stats](#retrieve-stats)
	- [Update stat](#update-stat)
	


# Mutation

## detect mutation



	POST /mutation


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| dna			| 			|  <p>Mutation's dna.</p>							|

# Stat

## Create stat



	POST /stats


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| count_mutations			| 			|  <p>Stat's count_mutations.</p>							|
| count_no_mutation			| 			|  <p>Stat's count_no_mutation.</p>							|
| ratio			| 			|  <p>Stat's ratio.</p>							|

## Delete stat



	DELETE /stats/:id


## Retrieve stat



	GET /stats/:id


## Retrieve stats



	GET /stats


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update stat



	PUT /stats/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| count_mutations			| 			|  <p>Stat's count_mutations.</p>							|
| count_no_mutation			| 			|  <p>Stat's count_no_mutation.</p>							|
| ratio			| 			|  <p>Stat's ratio.</p>							|


