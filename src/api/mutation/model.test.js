import { Mutation } from '.'

let mutation

beforeEach(async () => {
  mutation = await Mutation.create({ dna: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = mutation.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mutation.id)
    expect(view.dna).toBe(mutation.dna)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = mutation.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mutation.id)
    expect(view.dna).toBe(mutation.dna)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
