import mongoose, { Schema } from 'mongoose'

const mutationSchema = new Schema({
  dna: [{
    type: String
  }],
  hasMutation: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

mutationSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      dna: this.dna,
      hasMutation: this.hasMutation,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Mutation', mutationSchema)

export const schema = model.schema
export default model
