import { Router } from 'express'
import { middleware as body } from 'bodymen'
import { middleware as query } from 'querymen'
import { detectMutation, show, index } from './controller'
import { schema } from './model'
export Mutation, { schema } from './model'

const router = new Router()
const { dna, hasMutation } = schema.tree

/**
 * @api {post} /mutation detect mutation
 * @apiName DetectMutation
 * @apiGroup Mutation
 * @apiParam dna Mutation's dna.
 * @apiSuccess {Object} mutation Mutation's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mutation not found.
 */
router.post('/',
  body({ dna, hasMutation }),
  detectMutation)

/**
 * @api {get} /mutation Retrieve mutation
 * @apiName RetrieveMutation
 * @apiGroup Stat
 * @apiUse listParams
 * @apiSuccess {Object[]} mutation List of mutation.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /mutation/:id Retrieve stat
 * @apiName RetrieveStat
 * @apiGroup Stat
 * @apiSuccess {Object} stat Stat's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Stat not found.
 */
router.get('/:id',
  show)

export default router
