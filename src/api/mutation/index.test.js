import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Mutation } from '.'

const app = () => express(apiRoot, routes)

let mutation

beforeEach(async () => {
  mutation = await Mutation.create({})
})

test('POST /mutation 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ dna: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.dna).toEqual('test')
})
