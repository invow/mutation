import { success, notFound } from '../../services/response/'
import { Mutation } from '.'
import { Stat } from '../stat/'

//import colors from 'colors'
var colors = require('colors/safe');

/**
 * @OriginalSample
const sample = [
  ['a', 't', 'g', 'c', 'g', 'a'],
  ['c', 'a', 'g', 't', 'g', 'c'],
  ['t', 't', 'a', 't', 't', 't'],
  ['a', 'g', 'a', 'c', 'g', 'g'],
  ['g', 'c', 'g', 't', 'c', 'a'],
  ['t', 'c', 'a', 'c', 't', 'g']
]
*/

/**
 * @Debug
 */
const debug = true

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Mutation.find(query, select, cursor)
    .then((mutation) => mutation.map((mutation) => mutation.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Mutation.findById(params.id)
    .then(notFound(res))
    .then((mutation) => mutation ? mutation.view() : null)
    .then(success(res))
    .catch(next)

export const detectMutation = ({ bodymen: { body } }, res, next) => {
  log(' ::: Sample IN ::: ', colors.grey(`${JSON.stringify(body)}`))
  const hasMutated = hasMutation(body, res)

  Stat.find()
  .then((stats) => {
    let _body = {
      count_mutations: hasMutated ? stats[0].count_mutations + 1 : stats[0].count_mutations,
      count_no_mutation: !hasMutated ? stats[0].count_no_mutation + 1 : stats[0].count_no_mutation,
      ratio: ((stats[0].count_mutations * 100) / (stats[0].count_no_mutation + stats[0].count_mutations)) / 100
    }
    Stat.findById(stats[0].id)
    .then(notFound(res))
    .then((stat) => stat ? Object.assign(stat, _body).save() : null)
  })
  body.hasMutation = hasMutated
  Mutation.create(body)
  .then((mutation) => mutation.view(true))
  .then(success(res, 201))
  .catch(next)
} 

/**
 * Transform to Matrix
 * Convert the input data into a Matrix
 * @param {Array<String>} arr 
 */
const transform2Matrix = (arr) => {
  let matrix = [];
  for(let i = 0; i < arr.length; i++) {
    const row = arr[i].toLowerCase().split('')
    matrix[i] = row
  }
  return matrix
}


/**
 * Letters Counter
 * @param {Array<String>} arrStr 
 */
const lettersCounter = (arrStr) => {
  let s = null
  let results = 0
  if(arrStr[0].length === 1) {
    arrStr = arrStr.join('')
    arrStr = [arrStr]
  }
  arrStr.forEach(item => {
    s = item.match(/([a-zA-Z])\1*/g)||[]
    s.map((itm) => {
      if(itm.length >= 4) 
        results++
    })
  })
  //To return in matrix format: [${char}][${count}] >>>
  //you can use: [itm.charAt(0)][itm.length]
  return results
}

/**
 * Log
 * Evaluate const debug: boolean & prints in the console
 * @param {String} label 
 * @param {String} content 
 */
const log = (label, content) => {
  if(debug) console.log(`${label}\n`, content ? colors.green(content) : '')
}

/**
 * Log End
 * @param {Array<String>} vc: Vertical Chain 
 * @param {Array<String>} hc: Horizontal Chain 
 * @param {Number} mLength: Matrix length
 * @param {Number} i: Index
 */
const logEnd = (vc, hc, matrixLength, i) => {
  if(i + 1 === matrixLength) {
    log(' ::: Finished ::: ')
    log(' ::: Vertical Chain ::: ', colors.cyan(JSON.stringify(vc)))
    log(' ::: Horizontal Chain ::: ', colors.cyan(JSON.stringify(hc)))
  }
}

/**
 * Has Mutation
 * Detect mutations into the gen code
 * @param {Array<String>} dna 
 * @param {$Response>} res 
 */
const hasMutation = (sample, res) => {
  //Format: from Array<String> to a matrix[][]
  const matrix = transform2Matrix(sample.dna)
  log(' ::: MATRIX created ::: ', matrix)
  return read(matrix, res)
}

/**
 * Read
 * @param {Array<Array<String>>} matrix
 * @param {$Response>} res 
 */
const read = (matrix, res) => {
  let verticalChain = [],
    horizontalChain = [],
    diagFromZeroChain = [],
    diagFromLengthChain = [],
    currentVerticalMatrix = '',
    currentHorizontalMatrix = ''
  
  log(' ::: Reading ::: ')
  for(let i = 0; i < matrix.length ; i++) {
    for(let j = 0; j < matrix.length; j++) {
      //VERTICAL
      if(isValid(matrix[j][i])) {
        currentVerticalMatrix = currentVerticalMatrix + matrix[j][i]
        if(currentVerticalMatrix.length -1 === matrix.length - 1) {
          verticalChain.push(currentVerticalMatrix)
          currentVerticalMatrix = ''
        }
        //log(' ::: VERTICAL ::: ', colors.red(`${j}, ${i}`))
      } else return errorResponse(res)
      //HORIZONTAL
      if(isValid(matrix[i][j])) {
        currentHorizontalMatrix = currentHorizontalMatrix + matrix[i][j]
        if(currentHorizontalMatrix.length - 1 === matrix.length - 1) {
          horizontalChain.push(currentHorizontalMatrix)
          currentHorizontalMatrix = ''
        } 
        //log(' ::: HORIZONTAL ::: ', colors.red(`${i}, ${j}`))
      } else return errorResponse(res)
      //DIAGONAL FROM ZERO
      if(isValid(matrix[j][j])) {
        if(i === 0) diagFromZeroChain.push(matrix[j][j])
      } else return errorResponse(res)
      //DIAGONAL FROM LENGTH
      if(isValid(matrix[j][matrix.length - 1 - j])) {
        if(i === 0) diagFromLengthChain.push(matrix[j][matrix.length - 1 - j])
      } else return errorResponse(res)
    }
    logEnd(verticalChain, horizontalChain, matrix.length, i)
  }

  const verticalCounter = lettersCounter(verticalChain) ? lettersCounter(verticalChain) : 0
  const horizontalCounter = lettersCounter(horizontalChain) ? lettersCounter(horizontalChain) : 0
  const diagFromZeroCounter = lettersCounter(diagFromZeroChain) ? lettersCounter(diagFromZeroChain) : 0
  const diagFromLengthCounter = lettersCounter(diagFromLengthChain) ? lettersCounter(diagFromLengthChain) : 0

  const mutations = verticalCounter + horizontalCounter + diagFromZeroCounter + diagFromLengthCounter
  const mutation = (mutations > 1)

  log(' ::: Diag From Zero ::: ', colors.cyan(diagFromZeroChain))
  log(' ::: Diag From Length ::: ', colors.cyan(diagFromLengthChain))
  log(' ::: Mutations Counter ::: ', mutation ? colors.green(mutations) : colors.red(mutations))
  log(' ::: Has mutation? ::: ', mutation ? colors.green('true') : colors.red('false'))

  return mutation
}

const errorResponse = (res) => {
  res.status(403).json('The data entered is not valid for this analysis.')
}

/**
 * Is Valid
 * Check if the character is in the list of valid characters
 * @param {String} item 
 */
const isValid = (item) => {
  const valid = (
    item === 'a' ||
    item === 't' ||
    item === 'c' ||
    item === 'g'
  )
  return valid
}

