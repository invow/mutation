import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Stat, { schema } from './model'

const router = new Router()
const { count_mutations, count_no_mutation, ratio } = schema.tree

/**
 * @api {post} /stats Create stat
 * @apiName CreateStat
 * @apiGroup Stat
 * @apiParam count_mutations Stat's count_mutations.
 * @apiParam count_no_mutation Stat's count_no_mutation.
 * @apiParam ratio Stat's ratio.
 * @apiSuccess {Object} stat Stat's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Stat not found.
 */
router.post('/',
  body({ count_mutations, count_no_mutation, ratio }),
  create)

/**
 * @api {get} /stats Retrieve stats
 * @apiName RetrieveStats
 * @apiGroup Stat
 * @apiUse listParams
 * @apiSuccess {Object[]} stats List of stats.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /stats/:id Retrieve stat
 * @apiName RetrieveStat
 * @apiGroup Stat
 * @apiSuccess {Object} stat Stat's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Stat not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /stats/:id Update stat
 * @apiName UpdateStat
 * @apiGroup Stat
 * @apiParam count_mutations Stat's count_mutations.
 * @apiParam count_no_mutation Stat's count_no_mutation.
 * @apiParam ratio Stat's ratio.
 * @apiSuccess {Object} stat Stat's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Stat not found.
 */
router.put('/:id',
  body({ count_mutations, count_no_mutation, ratio }),
  update)

/**
 * @api {delete} /stats/:id Delete stat
 * @apiName DeleteStat
 * @apiGroup Stat
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Stat not found.
 */
router.delete('/:id',
  destroy)

export default router
