import { Stat } from '.'

let stat

beforeEach(async () => {
  stat = await Stat.create({ count_mutations: 'test', count_no_mutation: 'test', ratio: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = stat.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(stat.id)
    expect(view.count_mutations).toBe(stat.count_mutations)
    expect(view.count_no_mutation).toBe(stat.count_no_mutation)
    expect(view.ratio).toBe(stat.ratio)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = stat.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(stat.id)
    expect(view.count_mutations).toBe(stat.count_mutations)
    expect(view.count_no_mutation).toBe(stat.count_no_mutation)
    expect(view.ratio).toBe(stat.ratio)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
