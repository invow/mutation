import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Stat } from '.'

const app = () => express(apiRoot, routes)

let stat

beforeEach(async () => {
  stat = await Stat.create({})
})

test('POST /stats 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ count_mutations: 'test', count_no_mutation: 'test', ratio: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.count_mutations).toEqual('test')
  expect(body.count_no_mutation).toEqual('test')
  expect(body.ratio).toEqual('test')
})

test('GET /stats 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /stats/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${stat.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(stat.id)
})

test('GET /stats/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /stats/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${stat.id}`)
    .send({ count_mutations: 'test', count_no_mutation: 'test', ratio: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(stat.id)
  expect(body.count_mutations).toEqual('test')
  expect(body.count_no_mutation).toEqual('test')
  expect(body.ratio).toEqual('test')
})

test('PUT /stats/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ count_mutations: 'test', count_no_mutation: 'test', ratio: 'test' })
  expect(status).toBe(404)
})

test('DELETE /stats/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${stat.id}`)
  expect(status).toBe(204)
})

test('DELETE /stats/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
