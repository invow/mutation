import mongoose, { Schema } from 'mongoose'

const statSchema = new Schema({
  count_mutations: {
    type: Number
  },
  count_no_mutation: {
    type: Number
  },
  ratio: {
    type: Number
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

statSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      count_mutations: this.count_mutations,
      count_no_mutation: this.count_no_mutation,
      ratio: this.ratio,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Stat', statSchema)

export const schema = model.schema
export default model
