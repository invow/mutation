import { success, notFound } from '../../services/response/'
import { Stat } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Stat.create(body)
    .then((stat) => stat.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Stat.find(query, select, cursor)
    .then((stats) => stats.map((stat) => stat.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Stat.findById(params.id)
    .then(notFound(res))
    .then((stat) => stat ? stat.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Stat.findById(params.id)
    .then(notFound(res))
    .then((stat) => stat ? Object.assign(stat, body).save() : null)
    .then((stat) => stat ? stat.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Stat.findById(params.id)
    .then(notFound(res))
    .then((stat) => stat ? stat.remove() : null)
    .then(success(res, 204))
    .catch(next)
